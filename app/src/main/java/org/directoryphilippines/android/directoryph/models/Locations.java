package org.directoryphilippines.android.directoryph.models;

import org.directoryphilippines.android.directoryph.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by johneris on 10/7/15.
 */
public class Locations {

    private static List<Location> locationList;

    static {
        locationList = new ArrayList<>();

        Location location;

        location =  new Location("City A");

        location.addItem(new Item(Category.GOVERNMENT, "Government 1", "+639076492104", "some description", R.drawable.category_government));
        location.addItem(new Item(Category.GOVERNMENT, "Government 2", "+639076492104", "some description", R.drawable.category_government));

        location.addItem(new Item(Category.POLICE, "Police item 1", "+639076492104", "some description", R.drawable.category_police));
        location.addItem(new Item(Category.POLICE, "Police item 2", "+639076492104", "some description", R.drawable.category_police));

        location.addItem(new Item(Category.FIRE_RESPONSE, "Fire Response item 1", "+639076492104", "some description", R.drawable.category_fire));
        location.addItem(new Item(Category.FIRE_RESPONSE, "Fire Response item 2", "+639076492104", "some description", R.drawable.category_fire));

        location.addItem(new Item(Category.HOSPITAL, "Hospital item 1", "+639076492104", "some description", R.drawable.category_hospital));
        location.addItem(new Item(Category.HOSPITAL, "Hospital item 2", "+639076492104", "some description", R.drawable.category_hospital));

        locationList.add(location);


        location = new Location("City B");

        location.addItem(new Item(Category.GOVERNMENT, "Government 3", "+639076492104", "some description", R.drawable.category_government));
        location.addItem(new Item(Category.GOVERNMENT, "Government 4", "+639076492104", "some description", R.drawable.category_government));

        location.addItem(new Item(Category.POLICE, "Police item 3", "+639076492104", "some description", R.drawable.category_police));
        location.addItem(new Item(Category.POLICE, "Police item 4", "+639076492104", "some description", R.drawable.category_police));

        location.addItem(new Item(Category.FIRE_RESPONSE, "Fire Response item 3", "+639076492104", "some description", R.drawable.category_fire));
        location.addItem(new Item(Category.FIRE_RESPONSE, "Fire Response item 4", "+639076492104", "some description", R.drawable.category_fire));

        location.addItem(new Item(Category.HOSPITAL, "Hospital item 3", "+639076492104", "some description", R.drawable.category_hospital));
        location.addItem(new Item(Category.HOSPITAL, "Hospital item 4", "+639076492104", "some description", R.drawable.category_hospital));

        locationList.add(location);
    }

    public static List<Location> getLocationList() {
        return locationList;
    }

}
