package org.directoryphilippines.android.directoryph.models;

import android.support.annotation.DrawableRes;

import org.directoryphilippines.android.directoryph.R;

/**
 * Created by johneris on 10/7/15.
 */
public class Category {
    public static final int GOVERNMENT = 1;
    public static final int POLICE = 2;
    public static final int FIRE_RESPONSE = 3;
    public static final int HOSPITAL = 4;

    public static
    @DrawableRes
    int getImageRes(int category) {
        switch (category) {
            case GOVERNMENT:
                return R.drawable.category_government;
            case POLICE:
                return R.drawable.category_police;
            case FIRE_RESPONSE:
                return R.drawable.category_fire;
            case HOSPITAL:
                return R.drawable.category_hospital;
        }
        return 0;
    }

    public static String getCategoryTitle(int category) {
        switch (category) {
            case GOVERNMENT:
                return "Government";
            case POLICE:
                return "Police";
            case FIRE_RESPONSE:
                return "Fire Response";
            case HOSPITAL:
                return "Hospital";
        }
        return "";
    }
}
