package org.directoryphilippines.android.directoryph.ui.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import org.directoryphilippines.android.directoryph.R;
import org.directoryphilippines.android.directoryph.models.Item;

/**
 * Created by johneris on 10/7/15.
 */
public class ItemDetailActivity extends AppCompatActivity {

    private static final String ARGS_CATEGORY = "ARGS_CATEGORY";
    private static final String ARGS_IMAGE = "ARGS_IMAGE";
    private static final String ARGS_NAME = "ARGS_NAME";
    private static final String ARGS_CONTACT = "ARGS_CONTACT";
    private static final String ARGS_DESCRIPTION = "ARGS_DESCRIPTION";

    public static Intent newIntent(Context context, Item item) {
        Intent intent = new Intent(context, ItemDetailActivity.class);

        Bundle bundle = new Bundle();
        bundle.putInt(ARGS_CATEGORY, item.getCategory());
        bundle.putInt(ARGS_IMAGE, item.getImageRes());
        bundle.putString(ARGS_NAME, item.getName());
        bundle.putString(ARGS_CONTACT, item.getContact());
        bundle.putString(ARGS_DESCRIPTION, item.getDescription());

        intent.putExtras(bundle);

        return intent;
    }



    private Item mItem;

    private Toolbar mToolbar;

    private ImageView mImageView;
    private TextView mContactTextView;
    private TextView mDescriptionTextView;

    private Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_item_detail);

        Bundle bundle = getIntent().getExtras();

        mItem = new Item(
                bundle.getInt(ARGS_CATEGORY),
                bundle.getString(ARGS_NAME),
                bundle.getString(ARGS_CONTACT),
                bundle.getString(ARGS_DESCRIPTION),
                bundle.getInt(ARGS_IMAGE)
        );

        mContext = this;
        initUI();
    }

    private void initUI() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mToolbar.setTitle(mItem.getName());
        setSupportActionBar(mToolbar);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setHomeAsUpIndicator(R.drawable.ic_arrow_back_white_24dp);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        mImageView = (ImageView) findViewById(R.id.imageView);
        mContactTextView = (TextView) findViewById(R.id.contactTextView);
        mDescriptionTextView = (TextView) findViewById(R.id.descriptionTextView);

        mImageView.setImageResource(mItem.getImageRes());
        mContactTextView.setText(mItem.getContact());
        mDescriptionTextView.setText(mItem.getDescription());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return false;
    }

}
