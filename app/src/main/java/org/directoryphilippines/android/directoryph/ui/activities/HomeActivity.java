package org.directoryphilippines.android.directoryph.ui.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import org.directoryphilippines.android.directoryph.R;
import org.directoryphilippines.android.directoryph.models.Category;
import org.directoryphilippines.android.directoryph.ui.fragments.CategoryFragment;
import org.directoryphilippines.android.directoryph.ui.fragments.HomeFragment;

/**
 * Created by johneris on 10/7/15.
 */
public class HomeActivity extends AppCompatActivity {

    public static Intent newIntent(Context context) {
        Intent intent = new Intent(context, HomeActivity.class);
        return intent;
    }

    private NavigationView mNavigationView;
    private DrawerLayout mDrawerLayout;
    private int selectedMenuItem;

    private Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_home);
        mContext = this;

        initUI();
    }

    private void initUI() {
        mNavigationView = (NavigationView) findViewById(R.id.navigationView);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);

        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();

        HomeFragment homeFragment = HomeFragment.newInstance();
        fragmentTransaction.replace(R.id.container, homeFragment);
        fragmentTransaction.commit();

        selectedMenuItem = R.id.menuItemHome;
        mNavigationView.setCheckedItem(selectedMenuItem);

        mNavigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {
                menuItem.setChecked(true);
                mDrawerLayout.closeDrawer(GravityCompat.START);

                if (menuItem.getItemId() == selectedMenuItem) {
                    return true;
                }

                CategoryFragment categoryFragment;
                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();

                switch (menuItem.getItemId()) {
                    case R.id.menuItemHome:
                        HomeFragment homeFragment = HomeFragment.newInstance();
                        fragmentTransaction.replace(R.id.container, homeFragment);
                        fragmentTransaction.commit();
                        selectedMenuItem = menuItem.getItemId();
                        return true;
                    case R.id.menuItemGovernment:
                        categoryFragment = CategoryFragment.newInstance(Category.GOVERNMENT);
                        fragmentTransaction.replace(R.id.container, categoryFragment);
                        fragmentTransaction.commit();
                        selectedMenuItem = menuItem.getItemId();
                        return true;
                    case R.id.menuItemPolice:
                        categoryFragment = CategoryFragment.newInstance(Category.POLICE);
                        fragmentTransaction.replace(R.id.container, categoryFragment);
                        fragmentTransaction.commit();
                        selectedMenuItem = menuItem.getItemId();
                        return true;
                    case R.id.menuItemFireResponse:
                        categoryFragment = CategoryFragment.newInstance(Category.FIRE_RESPONSE);
                        fragmentTransaction.replace(R.id.container, categoryFragment);
                        fragmentTransaction.commit();
                        selectedMenuItem = menuItem.getItemId();
                        return true;
                    case R.id.menuItemHospital:
                        categoryFragment = CategoryFragment.newInstance(Category.HOSPITAL);
                        fragmentTransaction.replace(R.id.container, categoryFragment);
                        fragmentTransaction.commit();
                        selectedMenuItem = menuItem.getItemId();
                        return true;
                }

                return false;
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                mDrawerLayout.openDrawer(GravityCompat.START);
                return true;
        }
        return false;
    }

}
