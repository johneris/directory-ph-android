package org.directoryphilippines.android.directoryph.models;

import android.support.annotation.DrawableRes;

/**
 * Created by johneris on 10/7/15.
 */
public class Item {

    private int category;
    private String name;
    private String contact;
    private String description;
    private @DrawableRes int imageRes;

    public Item(int category, String name, String contact, String description, int imageRes) {
        this.category = category;
        this.name = name;
        this.contact = contact;
        this.description = description;
        this.imageRes = imageRes;
    }

    public int getCategory() {
        return category;
    }

    public String getName() {
        return name;
    }

    public String getContact() {
        return contact;
    }

    public String getDescription() {
        return description;
    }

    public int getImageRes() {
        return imageRes;
    }
}
