package org.directoryphilippines.android.directoryph.ui.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.directoryphilippines.android.directoryph.R;
import org.directoryphilippines.android.directoryph.ui.activities.HomeActivity;

public class HomeFragment extends Fragment {

    public static HomeFragment newInstance() {
        HomeFragment categoryFragment = new HomeFragment();
        return categoryFragment;
    }

    private Toolbar mToolbar;
    private Context mContext;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        mContext = getActivity();
        initUI(view);
        return view;
    }

    private void initUI(View view) {
        mToolbar = (Toolbar) view.findViewById(R.id.toolbar);
        mToolbar.setTitle("Home");

        if(getActivity() instanceof HomeActivity) {
            HomeActivity homeActivity = (HomeActivity) getActivity();
            homeActivity.setSupportActionBar(mToolbar);
            ActionBar actionBar = homeActivity.getSupportActionBar();
            if (actionBar != null) {
                actionBar.setHomeAsUpIndicator(R.drawable.ic_menu_white_24dp);
                actionBar.setDisplayHomeAsUpEnabled(true);
            }
        }

    }

}
