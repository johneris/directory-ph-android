package org.directoryphilippines.android.directoryph.models;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by johneris on 10/8/15.
 */
public class Location {

    private String name;

    // Integer - category
    // List<Item> - list of items
    private Map<Integer, List<Item>> itemsMap;



    public Location(String name) {
        this.name = name;
        itemsMap = new HashMap<>();
    }



    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Item> getItems(int category) {
        return itemsMap.containsKey(category) ?
                itemsMap.get(category) : itemsMap.get(category);
    }

    public void addItem(Item item) {
        List<Item> itemsForCategory;

        // if Map has already the key for category of item
        if(itemsMap.containsKey(item.getCategory())) {
            // get the current item list
            itemsForCategory = itemsMap.get(item.getCategory());
        } else {
            // create a new ArrayList of Item
            itemsForCategory = new ArrayList<>();
        }

        // add item to list
        itemsForCategory.add(item);

        // set the list of items be the value for key category
        itemsMap.put(item.getCategory(), itemsForCategory);
    }

}
