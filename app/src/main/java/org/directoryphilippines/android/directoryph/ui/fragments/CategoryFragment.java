package org.directoryphilippines.android.directoryph.ui.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import org.directoryphilippines.android.directoryph.R;
import org.directoryphilippines.android.directoryph.models.Category;
import org.directoryphilippines.android.directoryph.models.Item;
import org.directoryphilippines.android.directoryph.models.Location;
import org.directoryphilippines.android.directoryph.models.Locations;
import org.directoryphilippines.android.directoryph.ui.activities.HomeActivity;
import org.directoryphilippines.android.directoryph.ui.activities.ItemDetailActivity;

import java.util.ArrayList;
import java.util.List;

public class CategoryFragment extends Fragment {

    private static final String ARGS_CATEGORY = "ARGS_CATEGORY";

    public static CategoryFragment newInstance(int category) {
        CategoryFragment categoryFragment = new CategoryFragment();

        Bundle args = new Bundle();
        args.putInt(ARGS_CATEGORY, category);
        categoryFragment.setArguments(args);

        return categoryFragment;
    }

    private Toolbar mToolbar;
    private TextView mLocationTextView;
    private ImageView mCategoryImageView;
    private FloatingActionButton mFab;
    private ListView mItemsListView;

    private int mCategory;
    private List<Location> mLocations;
    private Location selectedLocation;

    private Context mContext;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_category, container, false);

        Bundle bundle = getArguments();
        mCategory = bundle.getInt(ARGS_CATEGORY);

        mContext = getActivity();

        mLocations = Locations.getLocationList();

        initUI(view);
        return view;
    }

    private void initUI(View view) {
        mToolbar = (Toolbar) view.findViewById(R.id.toolbar);
        mToolbar.setTitle(
                Category.getCategoryTitle(mCategory)
        );

        if (getActivity() instanceof HomeActivity) {
            HomeActivity homeActivity = (HomeActivity) getActivity();
            homeActivity.setSupportActionBar(mToolbar);
            ActionBar actionBar = homeActivity.getSupportActionBar();
            if (actionBar != null) {
                actionBar.setHomeAsUpIndicator(R.drawable.ic_menu_white_24dp);
                actionBar.setDisplayHomeAsUpEnabled(true);
            }
        }

        mLocationTextView = (TextView) view.findViewById(R.id.locationTextView);
        mLocationTextView.setText(mLocations.isEmpty() ? "" : mLocations.get(0).getName());
        mLocationTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showLocationsSpinner();
            }
        });

        mCategoryImageView =
                (ImageView) view.findViewById(R.id.categoryImageView);
        mCategoryImageView.setImageResource(
                Category.getImageRes(mCategory)
        );

        mFab = (FloatingActionButton) view.findViewById(R.id.fab);
        mFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showLocationsSpinner();
            }
        });

        mItemsListView = (ListView) view.findViewById(R.id.itemsListView);
        if (mLocations.isEmpty()) {
        } else {
            selectedLocation = mLocations.get(0);
            setItemsListView();
        }

        mItemsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Item item = selectedLocation.getItems(mCategory).get(position);
                Intent intent = ItemDetailActivity.newIntent(mContext, item);
                mContext.startActivity(intent);
            }
        });
    }

    private void showLocationsSpinner() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(mContext);

        View dialogLocationsView = LayoutInflater.from(mContext)
                .inflate(R.layout.dialog_locations, null);
        ListView locationsListView = (ListView) dialogLocationsView.findViewById(R.id.locationsListView);

        builder.setView(dialogLocationsView);

        final AlertDialog alertDialog = builder.create();

        List<String> locationsNames = new ArrayList<>();
        for (int i = 0; i < mLocations.size(); i++) {
            locationsNames.add(mLocations.get(i).getName());
        }

        locationsListView.setAdapter(
                new ArrayAdapter<>(mContext,
                        android.R.layout.simple_list_item_1,
                        locationsNames)
        );

        locationsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                selectedLocation = mLocations.get(position);
                String location = selectedLocation.getName();
                mLocationTextView.setText(location);
                setItemsListView();
                alertDialog.dismiss();
            }
        });

        alertDialog.show();
    }

    private void setItemsListView() {
        List<String> itemNames = new ArrayList<>();
        for (Item item : selectedLocation.getItems(mCategory)) {
            itemNames.add(item.getName());
        }
        mItemsListView.setAdapter(
                new ArrayAdapter<>(mContext,
                        android.R.layout.simple_list_item_1,
                        itemNames)
        );
    }

}
